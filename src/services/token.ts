import {Injectable} from '@angular/core';

@Injectable()

export class TokenService {
  
    private api_key: string;

    /**
     * Create header globally to call API Rest
     */
    prepareHeader(): object 
    {
      this.api_key = "394772d23dfb455a9fc5ee31ce8ee53a"; 
      let headers = {
        'Content-Type': 'application/json',
        'accessKey' : this.api_key
      };
      return headers;
    }
}