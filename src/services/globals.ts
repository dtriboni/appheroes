import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { TokenService } from './token';
import { HTTP } from '@ionic-native/http';

@Injectable()

export class GlobalVars {
  
  public status: boolean = false;
  public networkDown: boolean = false;
  public colorDefault: string = "primary";
  public itemOnDemand: boolean = false;
  public lbl_item_demand: string = "";
  public arrHero = [];
  public hero_items = [];
  public uploadEnded: boolean = false;

  public baseUrl =  "https://heroes.globalthings.net/api";

  constructor(private storage: Storage, 
              private network: Network,
              private token: TokenService,
              private http: HTTP,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController) { 
    
      /**
       * Show Toast and NotConnection View if
       * Internet connections was down.
       */
      this.network.onDisconnect().subscribe(() => {
        if (!this.networkDown)
        {
          this.presentToast('Você está operando em modo offline!');
          this.networkDown = true;
          this.colorDefault = "dark";
        }
      });

      
      /**
       * Show Toast and disable NotConnection View if
       * Internet connections is restabilished.
       */
      this.network.onConnect().subscribe(() => {
        if (this.networkDown)
        {
          this.presentToast('Opa, você está online novamente!');
          this.networkDown = false;
          this.colorDefault = "primary";
          this.get();
        }
      });
        
      this.get(); 
  }


  /**
   * get base url globally
   */
  getBaseUrl() {
    return this.baseUrl;
  }


  /**
   * Save hero offline in storage data
   * @param name 
   * @param categoryId 
   * @param active 
   */
  addHeroDemand(name: string, categoryId: number, active: boolean = true){
    var uuid = this.uuid();
    var objJson = {
      Id: "",
      Name: "",
      CategoryId: 0,
      Active: false
    };
    objJson.Id = "h-" + uuid;
    objJson.Name = name;
    objJson.CategoryId = categoryId;
    objJson.Active = active;
    this.save("h-" + uuid, objJson);
    this.get();
  }


  /**
   * Save in storage (called by addHeroDemand)
   * @param key 
   * @param arrHero 
   */
  private save(key: string, arrHero: any) {
    this.storage.set(key, arrHero); 
  }


  /**
   * Remove hero from storage and refresh offline content
   * @param key 
   */
  remove(key: string) {
    this.storage.remove(key);
    console.warn('One Item removed: ' + key);
    this.get();
  }


  /**
   * Create UUID (Universally Unique IDentifier) to Hero Id - RFC-4122
   */
  uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }


  /**
   * Get Offline contents by Storage data
   */
  get(){
    setTimeout( () => {
      this.hero_items = [];
      var c = 0;
      var vlitem = "";
      return this.storage.forEach( (value: string, key: string, iterationNumber: Number) => {
        if (value !== undefined){
          var only_cart_keys = key.split("-", 1);
          if (only_cart_keys[0] === "h"){
            c++;
            var json = JSON.parse(JSON.stringify(value));
            alert(JSON.stringify(value));
            vlitem = (c > 1 ? c + " heróis salvos" : "Um herói salvo");
            if (this.networkDown == false)
            {
              let loading = this.loadingCtrl.create({
                spinner: 'crescent',
                content: 'Salvando herói ' + json.Name + '. Aguarde...'});
                loading.present(); 
                var onDemand = { Name: json.Name, 
                                 CategoryId: json.CategoryId, 
                                 Active: json.Active };
                this.create(onDemand).then(data => {
                if (data.status == 201)
                {
                  this.remove(json.Id);
                  this.uploadEnded = true;
                }else{
                  this.presentToast("Falha ao salvar " + json.Name);
                }
                loading.dismiss();
                }).catch(error => {
                  loading.dismiss();
                  this.presentToast("Falhou ao salvar " + json.Name);
                });
            }
            this.hero_items.push(json);
          }
        }
      })
      .then(() => {
        this.lbl_item_demand = vlitem;
        if (c > 0){
          this.itemOnDemand = true;
        }else{
          this.itemOnDemand = false;
        }  
        return Promise.resolve(this.hero_items);
      })
      .catch((error) => {
        return Promise.reject(error);
      });
    }, 200);
  }


  /**
   * Save User data to API after device is connected
   * @param register 
   */
  create(register: { Name: string, 
                    CategoryId: number, 
                    Active: boolean}): Promise<any> {
      const expandedHeaders = this.token.prepareHeader();
      this.http.setDataSerializer('json');
        return this.http.post(this.getBaseUrl() + '/Heroes', register, expandedHeaders)
          .then(data => data)
          .catch(error => error);
  }


  /**
   * Show messages on footer device (Toast)
   * @param msgToast 
   */
  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast'); 
    });
    toast.present();
  }
}