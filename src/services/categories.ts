import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { TokenService } from './token';
import { GlobalVars } from './globals';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

export interface CategoriesService {
  status: number;
  data: any;
}

@Injectable()

export class CategoriesService {

  constructor(private http: HTTP, 
              private token: TokenService, 
              private globals: GlobalVars) { }

  
  /**
   * Get categories list
   * call GET method from API
   */
  category(): Promise<any> {
    const expandedHeaders = this.token.prepareHeader();
    return this.http.get(this.globals.getBaseUrl() + '/Category', {}, expandedHeaders)
      .then(data => data)
      .catch(error => error);
  }


  /**
   * Get cached categories list
   * call GET method from API with observable
   */
  categories(): Observable<any> {
    const expandedHeaders = this.token.prepareHeader();
    return Observable.defer(() => {
      return this.http.get(this.globals.getBaseUrl() + '/Category', {}, expandedHeaders)
        .then(data => data)
        .catch(error => error);
    }).delay(1000).map(data => data);
  }
}
