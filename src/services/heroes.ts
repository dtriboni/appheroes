import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { TokenService } from './token';
import { GlobalVars } from './globals';

import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

export interface HeroesService {
  status: number;
  data: any;
}

@Injectable()

export class HeroesService {

  constructor(private http: HTTP, 
              private token: TokenService, 
              private globals: GlobalVars) { }


  /**
   * Get heroes list
   * call GET method from API
   * @param register 
   */
  get(): Promise<any> {
    const expandedHeaders = this.token.prepareHeader();
    return this.http.get(this.globals.getBaseUrl() + '/Heroes', {}, expandedHeaders)
      .then(data => data)
      .catch(error => error);
  }


  /**
   * Delete current hero
   * call DELETE method from API
   * @param register 
   */
  delete(id: number): Promise<any> {
    const expandedHeaders = this.token.prepareHeader();
    return this.http.delete(this.globals.getBaseUrl() + '/Heroes/' + id, {}, expandedHeaders)
      .then(data => data)
      .catch(error => error);
  }


  /**
   * Create new hero (returns 201 method - CREATED)
   * call POST method from API
   * @param register 
   */
  create(register: { Name: string, 
                     CategoryId: number, 
                     Active: boolean}): Promise<any> {
    const expandedHeaders = this.token.prepareHeader();
    this.http.setDataSerializer('json');
    return this.http.post(this.globals.getBaseUrl() + '/Heroes', register, expandedHeaders)
      .then(data => data)
      .catch(error => error);
  }


  /**
   * Update hero data
   * call PUT method from API
   * @param register 
   */
  update(register: { Id: number,
                     Name: string, 
                     CategoryId: number, 
                     Active: boolean }): Promise<any> {
                  const expandedHeaders = this.token.prepareHeader();
                  return this.http.put(this.globals.getBaseUrl() + '/Heroes/' + register.Id, register, expandedHeaders)
                  .then(data => data)
                  .catch(error => error);
  }

}
