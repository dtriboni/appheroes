import { Component } from '@angular/core';
import { NavController, LoadingController, Refresher } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { CategoriesService } from '../../services/categories';
import { CacheService } from '../../app/cache.service';
import { Cache } from 'ionic-cache-observable';
import { GlobalVars } from '../../services/globals';

@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})

export class CategoriesPage {

  private listCategories: any;
  private no_items: boolean = false;
  private cache: Cache<CategoriesService>;

  constructor(public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              public cacheService: CacheService,
              public globals: GlobalVars, 
              public categories: CategoriesService) {}



  /**
   * Refresh cached content
   * @param refresher 
   */
  doRefresh(refresher: Refresher): void {
    // Check if the cache has registered.
    if (this.cache) {
      this.cache.refresh().subscribe(() => {
        // Refresh completed, complete the refresher animation.
        refresher.complete();
      }, (err) => {
        // Something went wrong! 
        // Log the error and cancel refresher animation.
        refresher.cancel();
      });
    } else {
      // Cache is not registered yet, so cancel refresher animation.
      refresher.cancel();
    }
  }


   /**
   * Prepare cached data before view enter
   */
  ionViewWillEnter()
  {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando categorias...'});
      loading.present();
      let categoryListObservable: Observable<CategoriesService> = this.categories.categories();
      this.cacheService
      .register('home_cats_11', categoryListObservable)
      .mergeMap((cache: Cache<CategoriesService>) => cache.get())
      .subscribe((categories) => {
        if (categories.status == 200)
        {
          this.listCategories = JSON.parse(categories.data); 
        }else{
          this.globals.presentToast("Não foi possível obter as categorias dos heróis.");
        }
        loading.dismiss();
    }); 
  }
}