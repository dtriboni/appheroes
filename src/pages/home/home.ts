import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { GlobalVars } from '../../services/globals';
import { HeroesService } from '../../services/heroes';
import { SignupPage } from '../signup/signup';
import { CategoriesService } from '../../services/categories';
import { CacheService } from '../../app/cache.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public heroesList = [];
  public categoryList = [];
  public arrCategory = [];
  private no_items: boolean = false;

  constructor(public navCtrl: NavController, 
              private globals: GlobalVars,
              private storage: Storage,
              private loadingCtrl: LoadingController,
              public cacheService: CacheService,
              private heroes: HeroesService,
              private category: CategoriesService,) {}


  /**
   * Refresh home after offline contents are uploaded
   */            
  doRefresh(refresher) 
  {
    this.ionViewWillEnter();
    this.globals.uploadEnded = false;
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }


  /**
   * Prepare heroes and categories from API GET before view enter
   */
  ionViewWillEnter()
  {
    let loading = this.loadingCtrl.create(
    { 
      spinner: 'crescent', 
      content: 'Buscando heróis...'
    });
    loading.present();

    //retrieve category names by API  to perform an new array to list
    this.category.category().then(data => 
    {
      if (data.status == 200)
      {
        this.arrCategory = JSON.parse(data.data); 
        this.arrCategory.forEach(element => 
        {
          this.categoryList[element.Id] = element.Name;
        }) 
      }
    }).catch(error => { console.log(error) }); 

    //retrieve heroes list by API
    this.heroes.get().then(data => 
    {
      if (data.status == 200)
      {
        this.heroesList = JSON.parse(data.data);
        this.no_items = false;

      }else{
        if (this.globals.networkDown == false)
        {
          this.globals.presentToast("Não foi possível exibir os heróis!");  
          this.no_items = true;
        }else{
          this.no_items = false;
        }

        this.heroesList = [];
      }
      loading.dismiss();

    }).catch(error => { this.heroesList = [] }); 
  }


  /**
   * Call hero form to update data
   * @param hero 
   */
  updateHero(hero: any)
  {
    this.navCtrl.push(SignupPage, { heroId: hero.Id, 
                                    heroName: hero.Name, 
                                    heroCategoryId: hero.CategoryId, 
                                    heroActive: hero.Active });
  }


  /**
   * Call delete method from API to remove hero
   * reload contents in home
   * @param id 
   */
  removeHero(id: number)
  {
    if (confirm("Deseja remover este herói da lista?"))
    {
      this.heroes.delete(id).then(data => 
      {
        if (data.status == 200)
        {
          this.ionViewWillEnter();

        }else{

          this.globals.presentToast("Não foi possível remover o herói no momento.");
        }

      }).catch(error => 
      {
        this.globals.presentToast("Não foi possível remover o herói no momento");
      });
    }
  }


  /**
   * Remove offline hero if device is disconnected
   * reload contents in home
   * @param id 
   */
  removeOffline(id: string)
  {
    if (confirm("Deseja remover este herói da lista?"))
    {
      this.globals.remove(id);
    }
  }
}