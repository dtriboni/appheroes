import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { CategoriesPage } from '../categories/categories';
import { GlobalVars } from '../../services/globals';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SignupPage;
  tab3Root = CategoriesPage;

  constructor(public globals: GlobalVars) {}
}
