import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalVars } from '../../services/globals';
import { HeroesService } from '../../services/heroes';
import { Observable } from 'rxjs/Observable';
import { CacheService } from '../../app/cache.service';
import { Cache } from 'ionic-cache-observable';
import { CategoriesService } from '../../services/categories';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignupPage {

  private form: FormGroup;
  private listCategories = [];
  private submitAttempt: boolean = false;
  private heroId: string = "0";
  private heroName: string;
  private heroCategoryId: number;
  private heroActive: string;

  constructor(public navCtrl: NavController, 
              private formBuilder: FormBuilder,
              private loadingCtrl: LoadingController, 
              private categories: CategoriesService,
              public cacheService: CacheService,
              private storage: Storage,
              private globals: GlobalVars,
              private heroes: HeroesService,
              public navParams: NavParams) {

    //retrieve hero data to update
    var hID = this.navParams.get('heroId');
    this.heroId = (hID != undefined ? hID : "0");
    this.heroName = this.navParams.get('heroName');
    this.heroCategoryId = this.navParams.get('heroCategoryId');
    this.heroActive = this.navParams.get('heroActive');

    this.form = this.formBuilder.group({
      Name: [this.heroName, Validators.compose([Validators.maxLength(20), Validators.required])],
      CategoryId: [this.heroCategoryId, Validators.compose([Validators.required])],
      Active: [this.heroActive, Validators.compose([Validators.required])]
    });

  }


  /**
   * Prepare cached data before view enter
   */
  ionViewWillEnter()
  {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando categorias...'});
    loading.present();
      let categoryListObservable: Observable<CategoriesService> = this.categories.categories();
      this.cacheService
        .register('home_cats_11', categoryListObservable)
        .mergeMap((cache: Cache<CategoriesService>) => cache.get())
        .subscribe((categories) => {
          if (categories.status == 200)
          {
            this.listCategories = JSON.parse(categories.data); 
          }else{
            this.globals.presentToast("Não foi possível obter as categorias dos heróis.");
          }
          loading.dismiss();
      }); 
  }


  /**
   * Clear form fields
   */
  private clearFields()
  {
    let that = this;
    that.form.controls["Name"].setValue("");
    that.form.controls["CategoryId"].setValue("");
    that.form.controls["Active"].setValue("");
  }


  /**
   * Save or Update Hero data
   */
  signUp()
  {
    this.submitAttempt = true;
    
    if(this.form.valid)
    {
      let loading = this.loadingCtrl.create({
          spinner: 'crescent',
          content: 'Por favor, aguarde...'});
      loading.present(); 
      
      if (this.heroId != "0")
      {
        // check if device is not Connected
        // if true, skip update
        if (this.globals.networkDown)
        {
          loading.dismiss();
          this.storage.remove(this.heroId);
          this.globals.addHeroDemand(this.form.value.Name, this.form.value.CategoryId, this.form.value.Active);
          this.globals.presentToast("Seu Herói foi atualizado offline temporariamente!");
          this.navCtrl.parent.select(0);

        }else{ //if connected, save in API

          this.form.value.Id = this.heroId;
          this.heroes.update(this.form.value).then(data => {
            if (data.status == 200)
            {
              this.globals.presentToast("Seu Herói foi alterado com sucesso!");
              this.clearFields();
              this.navCtrl.parent.select(0);

            }else{
              this.globals.presentToast("Não foi possível alterar o herói no momento.");
            }
            loading.dismiss();
    
          }).catch(error => {
            loading.dismiss();
            this.globals.presentToast("Não foi possível alterar o herói no momento");
          });
        }
        
      }else{

        // check if device is not Connected
        // if true, save hero offline
        if (this.globals.networkDown)
        {
          loading.dismiss();
          this.globals.addHeroDemand(this.form.value.Name, this.form.value.CategoryId, this.form.value.Active);
          this.globals.presentToast("Seu Herói foi salvo offline temporariamente!");
          this.clearFields();
          this.navCtrl.parent.select(0);

        }else{ //if connected, save in API

          this.heroes.create(this.form.value).then(data => 
          {
            if (data.status == 201)
            {
              this.globals.presentToast("Seu Herói foi incluído com sucesso!");
              this.clearFields();
              this.navCtrl.parent.select(0);

            }else{
              this.globals.presentToast("Não foi possível incluir o herói no momento.");
            }
            loading.dismiss();
    
          }).catch(error => 
          {
            loading.dismiss();
            this.globals.presentToast("Não foi possível incluir o herói no momento");
          });

        }
      }
    }
  }
}
