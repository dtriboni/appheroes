import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { CategoriesPage } from '../pages/categories/categories';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoriesService } from '../services/categories';
import { HeroesService } from '../services/heroes';
import { TokenService } from '../services/token';
import { HTTP } from '@ionic-native/http';
import { IonicStorageModule } from '@ionic/storage';
import { GlobalVars } from '../services/globals';
import { CacheModule } from "ionic-cache-observable";
import { CacheService } from './cache.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    CategoriesPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot({
      name: '__gtheroesdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicModule.forRoot(MyApp),
    CacheModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    CategoriesPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    Network,
    HTTP,
    CacheService,
    TokenService,
    CategoriesService,
    HeroesService,
    GlobalVars,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
